[![Docs](https://img.shields.io/badge/docs-via%20gitlab%20pages-success)](https://open-rit.gitlab.io/mystic/mystic.html)

# Mystic

Mystic is your hub for open source everything.  Community is central to every open source project, whether it's open source, open science, or anything else in the open ecosystem.  Mystic is your tool to build that community.  Anyone in the community can submit what they've been working on and immediately have access to a homepage for their project, [CHAOSS] community/software health metrics, and visibility within the community — letting them do amazing work among a strong community.

**MYSTIC IS NOT YET SUITABLE TO BE RUN IN A PRODUCTION ENVIRONMENT**

## Configuration

Mystic is configured through a config file, by default mystic.cfg, although by setting `MYSTIC_CONFIG` other config files can be set.

An example config file can be found in `config.example.yml`.  You are encouraged to copy this file to `config.yml` and use it as a base.

All configuration options are case sensitive and must be in all capitals.  The
following configuration options are available:

* **DATABASE**:  Information about the MySQL database to connect to.  See the Database section
* **PROJECTS_FILE**:  Mystic outputs a json file compatible with grimoirelab's
  `projecs.json` file, the location been can be set through this file
* **SAML_\***: See the section on SAML
* **SECRET_KEY**: Set this to some random string, and keep it secure
* **SERVER_NAME**: The domain that the server will be served on (optional)

## Database

Mystic was designed to run with a MySQL database, so that it could be efficiently paired with the grimoirelab stack.  The connection details can be specified through a dictionary passed through the **DATABASE** config option.  This dictionary needs the following keys:

* **host**:  The hostname of the server the MySQL database is running on.  This assumes port 3306.
* **user**:  The user that mystic should connect as.  Should not be `root`.
* **password**:  The database password to use when connecting.  The password of the user defined above
* **database**:  The database to store data in.

For development and self-hosting, a docker-compose file is provided (`development-database.docker-compose.yml`) that can be used to easily set up a database without any fiddling.  To do this, simply:

* Install [`docker`][Install Docker] and [`docker-compose`][Install docker-compose] on your system
* Start the docker daemon (typically, this means running `sudo systemctl start docker`)
* Run the docker compose file (`sudo docker-compose up -f development-database.docker-compose.yml -d`)

The default config is already configured to be compatible with the development instance that this should create, so no further changes to the database config are required.

## SAML

Mystic was designed to be run with SAML authentication.  Future authentication schemes are planned, but not yet implemented.  For now, SAML is configured as follows:

**SAML_SP** should be configured to point to a private key and certificate.  If open SSL is installed, these can be generated with the command

```bash
openssl req -x509 -newkey rsa:4096 -keyout sp_private_key.pem -out sp_certificate.pem -nodes -days 900
```

The SAML2_IDENTITY_PROVIDERS should contain a list of identity providers for users to login with.  Typically, you only need one of these.

* **CLASS** option can typically be left as `flask_saml2.sp.IdPHandler`, unless you need a specialized backend to handle logins
* **display_name** should be set to a human readable description for the identity providers
* **entity_id** should be the entity ID set by the IDP
* **sso_url** and **slo_url** should be set to the single sign on an single log out url endpoints defined by the IDP
* **certificate** should point to a copy of the certificate used by the IDP


## Running

Mystic can be run using Docker either standalone or with the rest of the Grimoire stack.  Currently, only development releases are being posted, and are generated through GitLab's CI.  Once Mystic releases properly, release builds will be available.

### Standalone

Mystic's container can be pulled from the GitLab container registry at `registry.gitlab.com/open-rit/mystic:dev` (for the development branch).  The following mounts/volumes are recommended:
- `/data/config.cfg`: Mounting the main config file
- `/data/keys`: To store private & public keys for SAML
- `/data/projects.json`: To retrieve the `projects.json` file, which must be fed to Grimoirelab in order to generate metrics

Of course, you will still need to run the rest of the grimoirelab stack in order to have complete functionality, including a MySQL server, grimoirelab-sirmordred, and elasticsearch, at minimum.  For this reason, it's recommended that you deploy the full stack.

### Full Stack

Mystic also offers a docker-compose file for deploying the entire software stack needed to run Mystic, as well as configuration files for its components.  To see this, check out the `stack` folder of this repository.

### Development

Mystic is a Flask app, so running a development build is as simple as:

```bash
python3 -m venv venv
source bin/activate
pip install -e .
set -x FLASK_APP mystic
flask run
```

## Credits

This project uses some icons by [Feather Icons][], which are made available under the [MIT License][]

This project also uses some icons by [Remix Icon][], which are made available under the [Apache License][]

[GrimoireLab]: https://chaoss.github.io/grimoirelab/
[Feather Icons]: https://feathericons.com/
[MIT License]: https://github.com/feathericons/feather/blob/master/LICENSE
[Install Docker]: https://docs.docker.com/engine/install/ "Docker installation guide"
[Install docker-compose]: https://docs.docker.com/compose/install/ "docker-compose installation guide"
[Chaoss]: https://chaoss.community/metrics/ "Chaoss Community: Metrics"
[Remix Icon]: https://github.com/Remix-Design/remixicon
[Apache License]: https://github.com/Remix-Design/remixicon/blob/master/License
