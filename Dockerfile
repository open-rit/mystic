FROM python:3-buster
LABEL org.opencontainers.image.authors="Emi Simpson <emi@mail.rit.edu>"

# Install some dependencies
RUN python -m venv .
RUN bin/pip install waitress

# Setup /data
RUN mkdir /data
COPY stack/mystic/config.cfg /data/config.cfg
VOLUME /data

# Configuration
ENV MYSTIC_CONFIG /data/config.cfg
ENV FLASK_APP mystic
EXPOSE 80/tcp
ENTRYPOINT ["bin/waitress-serve"]
CMD ["--trusted-proxy", "*", "--listen", "*:80", "wsgi:app"]

# Load remaining files
COPY pyproject.toml pyproject.toml
COPY setup.py setup.py
COPY docker_data/wsgi.py .
COPY mystic mystic

# Install
RUN bin/pip install -e .

# Setup database
RUN bin/python mystic/init_database.py /data/database.db
