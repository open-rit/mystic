# The Mystic Software Stack

Everything you need to install mystic + the grimoirelab stack

Mystic and grimoirelab collectively have a pretty big stack of software they
depend on.  Often times, it can be very intimidating to try to set it all up.
We aim to make this process as simple as it can be!

## Installation

### Step 0: Install dependencies

You'll need a couple of programs in order to follow this guide, namely:
- [`docker-compose`][3]
- `openssl` (probably pre-installed)
- `git` (should already be installed)
- A text editor you're comfortable with

### Step 1: Clone the repository

Download all the code you need with a quick `git clone`

```bash
git clone https://gitlab.com/open-rit/mystic.git
cd mystic/stack # Checkout this directory
```

### Step 2: Complete configuration

You'll need to swap out the <CONFIGURATION PLACEHOLDERS> (written like that) in the various config files.  Specifically:

- In `build/kibiter/kibana.yml`:
  - Replace `<YOUR DOMAIN HERE>` with the domain that kibiter will be running on
- In `caddy/Caddyfile`:
  - Replace `<YOUR EMAIL HERE>` with your email.  This will be used to register a LetsEncrypt SSL certificate
- In `mariadb/init-scripts/50-setup-apps.sql`:
  - Replace `<SECURE MYSQL PASSWORD HERE>` and `<MORDREDS SECURE MYSQL PASSWORD
	HERE>` with two securely generated passwords
  - Hang on to those passwords, because we'll need them in a bit.
- In `mordred/configs/setup-secured.cfg`:
  - Replace `<MORDREDS SECURE MYSQL PASSWORD HERE>` with the password from the last step
  - Scroll down and enable the sources you want to support.  You'll need to
	provide your API key for some of them.  For more information about how to
	configure these, please see [the sirmordred documentation][1]
  - You may want to fill out other configuration files in `mordred/configs` in
	order to improve support for identities and organization, but this step is
	completely optional
- In `mystic/config.cfg`:
  - Replace `<SECURE MYSQL PASSWORD HERE>` with the password from earlier
  - Replace `<IDP DISPLAY NAME HERE>` with the display name of your identity
	provider.  That's the service that's providing your single sign on (SSO).
	This doesn't need to be anything flashy, just human readable.
  - Replace `<IDP ENTITY ID HERE>` with the entity ID of your IDP.  The admin
	of your IDP should be able to provide this for you, if you don't see it in
	your IDP metadata.
  - Replace `<SSO URL HERE>` with your IDP's SSO URL.  Your IDP should provide
	this value as well.
  - Replace `<CHANGE ME TO A SECURE RANDOM STRING>` to a secure random string.
    [Here's some sugguestions!][2]
  - Replace `<YOUR DOMAIN HERE>` to the domain name Mystic will be running on

### Step 3: Generate some keys

Create a new directory called `keys` in the `mystic` folder

```bash
mkdir mystic/keys
cd mystic/keys
```

Next, we need to generate some keys for mystic to use.  These will be used to
prove the identity of your server to your IDP.  We can use openssl to do this.

```bash
openssl req -x509 -newkey rsa:4096 -keyout sp_private_key.pem -out sp_certificate.pem -nodes -days 900
```

We'll also need the certificate for the IDP itself.  If your IDP doesn't provide this for you, you might be able to extract it from the `metadata.xml` of your IDP.  Just open up the metadata, and check for an `X509Certificate` tag.  Copy the contents of that tag into a new file, and add a line on the top that reads `-----BEGIN CERTIFICATE-----` and a line after it that reads `-----END CERTIFICATE-----`, so that the entire thing looks like

```
-----BEGIN CERTIFICATE-----
MIIELTCCAxWgAwIBAgIJAMc8ESDnsotiMA0GCSqGSIb3DQEBBQ...
-----END CERTIFICATE-----
```

### Step 4: Run this command

```bash
sudo sysctl -w vm.max_map_count=262144
```

I don't honestly understand what this means, but you [need it for elasticsearch to work][5]

### Step 5: Run the program!

Now just tell `docker-compose` to start up the stack!

```bash
cd ../.. # If you've been following along, you'll need to go back to the root
docker-compose up -d
```

[1]: https://github.com/chaoss/grimoirelab-sirmordred#supported-data-sources- "Grimoirelab Documentation"
[2]: https://www.random.org/passwords/?num=5&len=24&format=html&rnd=new "random.org passwords"
[3]: https://docs.docker.com/compose/install/
[4]: https://www.openssl.org/
[5]: https://github.com/chaoss/grimoirelab/blob/master/tests/README.md#running-the-containers "An exerpt from the grimoirelab docs"
