from pymysql.cursors import Cursor

def setup_database(c: Cursor) -> None:
	"""
	Set up the database tables

	Can be called multiple times on one database safely.  Will error if the database is
	misconfigured.
	"""
	script = '''
		CREATE TABLE IF NOT EXISTS users (
			user_id INTEGER PRIMARY KEY AUTO_INCREMENT,
			username VARCHAR(30) UNIQUE NOT NULL,
			first_name TINYTEXT NOT NULL,
			last_name TINYTEXT NOT NULL,
			email TINYTEXT NOT NULL
		);

		CREATE TABLE IF NOT EXISTS projects (
			project_id INTEGER PRIMARY KEY AUTO_INCREMENT,
			display_name VARCHAR(50) NOT NULL,
			description VARCHAR(500) NOT NULL,
			draft_owner INTEGER UNIQUE,
			FOREIGN KEY (draft_owner)
				REFERENCES users(user_id)
				ON DELETE CASCADE
				ON UPDATE RESTRICT
		);

		CREATE TABLE IF NOT EXISTS owners (
			user_id INTEGER NOT NULL,
			project_id INTEGER NOT NULL,
			PRIMARY KEY (user_id, project_id),
			FOREIGN KEY (user_id)
				REFERENCES users(user_id)
				ON DELETE CASCADE
				ON UPDATE RESTRICT,
			FOREIGN KEY (project_id)
				REFERENCES projects(project_id)
				ON DELETE CASCADE
				ON UPDATE RESTRICT
		);

		CREATE TABLE IF NOT EXISTS data_sources (
			source_id INTEGER PRIMARY KEY AUTO_INCREMENT,
			project_id INTEGER,
			data_type VARCHAR(50) NOT NULL,
			data_url VARCHAR(255) NOT NULL,
			FOREIGN KEY (project_id)
				REFERENCES projects(project_id)
				ON DELETE CASCADE
				ON UPDATE RESTRICT,
			UNIQUE (project_id, data_type, data_url)
		);

		CREATE INDEX IF NOT EXISTS dx_data_source_project ON data_sources(project_id);
		CREATE INDEX IF NOT EXISTS dx_project_draft_of ON projects(draft_owner);
	'''
	for stmt in script.split(';'):
		if len(stmt.strip()):
			c.execute(stmt, tuple())
