from _pytest.fixtures import fixture
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from flask import url_for
from sqlite3 import Connection

import pytest
from selenium.webdriver.firefox.webdriver import WebDriver

from typing import Any, Callable, Generator

def find_project(driver: WebDriver, project_name: str) -> WebElement:
	projects = driver.find_elements_by_class_name("project")
	try:
		return next(
			p
			for p
			in projects
			if p.find_element_by_css_selector("h5 > a:first-child").text == project_name
		)
	except StopIteration:
		raise RuntimeError("Project not found")

@fixture
def driver() -> Generator[WebDriver, None, None]:
	driver = webdriver.Firefox()
	yield driver
	driver.close()

@fixture
def login(driver: WebDriver, live_server: Any) -> WebDriver:
	driver.get(url_for('main.landing', _external=True))
	assert driver.title == 'Home - Mystic'
	username = driver.find_element_by_id('username')
	username.send_keys('Emi')
	password = driver.find_element_by_id('password')
	password.send_keys('PasswordIrrevelant')
	submit = driver.find_element_by_css_selector('input[value="Login"]')
	submit.click()
	assert driver.title == 'Projects - Mystic'
	return driver

@fixture
def add_project(driver: WebDriver, database_connection: Connection, login: None) -> Generator[Callable[[str], None], None, None]:
	projects = []
	def output_function(project_name: str) -> None:
		newnav = driver.find_element_by_css_selector('nav > a[href="/new"]')
		newnav.click()
		username = driver.find_element_by_id('name')
		username.send_keys(project_name)
		username.send_keys(Keys.ENTER)
		WebDriverWait(driver, 1).until(
			EC.title_is("Projects - Mystic")
		)
		projects.append(project_name)

	yield output_function

	for project in projects:
		c = database_connection.cursor()
		c.execute('DELETE FROM projects WHERE display_name = ?;', (project,))
		database_connection.commit()
		c.close()

@fixture
def add_source(driver: WebDriver) -> Callable[[str, str, str], None]:
	def output_function(project_name: str, src_type: str, src_url: str) -> None:
		if driver.title != 'Projects - Mystic':
			projnav = driver.find_element_by_css_selector('nav > a[href="/projects"]')
			projnav.click()
			WebDriverWait(driver, 1).until(
				EC.title_is("Projects - Mystic")
			)
		project = find_project(driver, project_name)
		form = project.find_element_by_class_name('addsource')
		select = Select(form.find_element_by_xpath("//select"))
		select.select_by_visible_text(src_type)
		url = form.find_element_by_xpath("//input[@name='url']")
		url.send_keys(src_url)
		submit = form.find_element_by_xpath("//input[@value='+']")
		submit.click()
	return output_function

@pytest.mark.usefixtures('login')
def test_login(driver: WebDriver) -> None:
	profile_button = driver.find_element_by_css_selector('nav > a[href="/account"]')
	assert profile_button.text == 'EMI'

def test_add_project(driver: WebDriver, add_project: Callable[[str], None]) -> None:
	add_project('Mystic')
	projects = driver.find_elements_by_css_selector('.project > h5 > a:first-child')
	assert len(projects) == 1
	assert projects[0].text == 'Mystic'

def test_add_source(
	driver: WebDriver,
	add_project: Callable[[str], None],
	add_source: Callable[[str, str, str], None]
) -> None:
	add_project('Mystic')
	add_source("Mystic", "GitHub Issues", "https://github.com/chaoss/grimoirelab-toolkit")
	add_source("Mystic", "GitHub Issues", "https://github.com/chaoss/grimoirelab-elk")
	add_source("Mystic", "Git Commits", "https://github.com/chaoss/grimoirelab-elk.git")
	project = find_project(driver, "Mystic")
