"""
Database models for storing application data

This module is dominated by two main models, `Project` and `User`, representing exactly
what you think they would.  All other tables in the database are represented as
attributes of these two models, or how they relate.

Importantly, models don't interact with the database unless explicitly told to.  This
means that when first instantiated, models contain only the ID of the database record they
are meant to represent.  More data can be loaded by calling `.load()` on the model, which
loads up any attributes included in the table as well as any 1:1 relations.  Other
relationships can be loaded by calling `.get_*` methods, which load a specific
relationship or attribute if not loaded already.

Similarly, any attempts to update fields of the model will not be processed until save()
is called.
"""
from hashlib import md5
from mystic.sources import SOURCE_PROCESSORS, Source
from functools import reduce

from typing import List, Optional, Dict, Tuple, Type, Union, cast

from pymysql.cursors import Cursor
from pymysql.err import IntegrityError

_alphabet = 'abcdefghijklmnopqrstuvwxyz'

class Project:
	def __init__(self, project_id: int) -> None:
		"""
		Initialize a new project model without any of the feilds initialized.
		Relevant fields will be filled lazilly as requested.

		This does not perform any database queries, nor does it validate
		whether the provided id exists in the database.  This can be manually
		checked by calling .load() on the new project.

		Raises:
			mystic.database.MalformedId: The ID of the project is invalid
		"""
		MalformedId.check_malformed(project_id)
		self._project_id:   int                     = project_id
		self._display_name: Optional[str]           = None
		self._description:  Optional[str]           = None
		self._is_draft:     Optional[bool]          = None
		self._draft_owner:  Optional[User]          = None
		self._modified:     bool                    = False
		self._owners:       Optional[List[User]]    = None
		self._data_sources: Optional[List[Source]]  = None

	def load(self, c: Cursor) -> None:
		"""
		Perform the necessary database query to complete some fields.

		This fills the display_name field, and in the future, likely some other
		fields that don't required complex database queries.  You will almost
		never have to call this method, as it is automatically called when a
		relevant and unfilled property is accessed.

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.NonexistantId:	The project was initialized with an ID
				that was not found
		"""
		c.execute('SELECT * FROM projects WHERE project_id = %s;', (self._project_id,))
		record = c.fetchone()
		if record is None:
			raise NonexistantId('project', self._project_id)
		self._display_name = record[1]
		self._description  = record[2]
		if record[3] is not None:
			# TODO load a full user object, not a partial one
			self._draft_owner = User(record[3])
			self._is_draft = True
		else:
			self._draft_owner = None
			self._is_draft = False

	def save(self, c: Cursor) -> None:
		"""
		Save any changes in this object to the database

		Unsaved changes created by setting :func:`description` or :func:`display_name` can
		be persisted to the database by calling :func:`save`.  This will overwrite any
		changes that have happened in the database since the last call to :func:`load`.

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.NonexistantId:	The project was initialized with an ID
				that was not found
		"""
		if self._modified:
			to_update: List[str]          = []
			values: List[Union[int, str]] = []
			if self._display_name is not None:
				to_update.append("display_name = %s")
				values.append(self._display_name)
			if self._description is not None:
				to_update.append("description = %s")
				values.append(self._description)
			if self._is_draft == False:
				to_update.append("draft_owner = NULL")
			fields = ','.join(to_update)
			self._modified = False
			values.append(self._project_id)

			c.execute(
				f'UPDATE projects SET {fields} WHERE project_id = %s',
				values
			)
			if c.rowcount == 0:
				raise NonexistantId('project', self._project_id)
			self._modified = False

	def add_owner(self, c: Cursor, user: 'User', update_user_model:bool = True) -> None:
		"""
		Add a user who is qualified to manage this project.

		Requires a database query.

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
			sqlite3.IntegrityError:  This user already owns this project
			mystic.database.NonexistantId:	Either the id of this project or
				the id of the provided user did not correspond to an actual
				user.  Which one can be determined by checking .item_type on
				the exception
		"""
		try:
			c.execute(
				'INSERT INTO owners VALUES (%s, %s);',
				(user.user_id, self._project_id)
			)
		except IntegrityError as e:
			if e.args[0] != 1452: # IntegrityError
				raise e
			elif not self.loaded and user.loaded:
				# Attempt to guess if only the project hasnt been validated
				raise NonexistantId("project", self._project_id)
			elif self.loaded and not user.loaded:
				# Attempt to guess if only the user hasnt been validated
				raise NonexistantId("user", user.user_id)
			else:
				self.load(c) # Verify the project, errors if fails
				raise NonexistantId("user", user.user_id) # Only other possibility is the user
		if self._owners is not None:
			self._owners.append(user)
		if user.projects_loaded and update_user_model:
			user.get_projects(c).append(self)

	def remove_owner(self, c: Cursor, owner: 'User') -> bool:
		"""
		Remove an owner from this project.

		Requires a database query

		If either the user or the project does not exist, this returns false.  If the user
		does not own the project, this returns false.  If the user did own the project,
		and was successfully removed, this returns true.

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
		"""
		row_count = c.execute(
			'DELETE FROM owners WHERE user_id = %s AND project_id = %s;',
			(owner.user_id, self._project_id)
		)
		return row_count != 0

	def add_data_source(self, c: Cursor, source_type: str, source_url: str) -> int:
		"""
		Add a data source to the project

		A data source consists of a type, such as github, git, gitlab, etc, and
		a URL.	The type is one of a pre-defined list of valid types, and the
		url identifies where perceval will look for the data.  Each project can
		have many different sources, and even several of each type.  Perceval
		collects data from each source, and compiles it into the end
		visualization.

		This method does not validate the data source at all, and blindly
		inserts it into the database.  Validation must be done by the calling
		method, if necessary.

		If successful, the source id of the new source is returned

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
			sqlite3.IntegrityError:  An identical data source already exists for
				this project
			mystic.database.NonexistantId:	The project model was configured
				with an id that doesn't match any projects in the database
		"""
		try:
			c.execute('''
				INSERT INTO data_sources (
					data_type,
					data_url,
					project_id
				)
				VALUES (%s, %s, %s);
			''', (source_type, source_url, self._project_id))
		except IntegrityError as e:
			if e.args[0] != 1452:
				raise e
			else:
				raise NonexistantId("project", self._project_id)
		source = Source(c.lastrowid, self, source_type, source_url)
		if self._data_sources is not None:
			self._data_sources.append(source)
		return source.origin_id

	def finalize_draft(self, c: Cursor, project_name: str, description: str) -> None:
		"""
		Finalize a draft project, turning it into a real project

		A draft project (see :func:`draft_owner`) can be converted into a real project by
		giving it a name and a description with this method.  Once this is done, the
		project will be visible to all owners in their project listings, and will no
		longer be a draft of the previous owner.

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.NonexistantId:	The project was initialized with an ID
				that was not found
		"""
		assert self._draft_owner is not None
		self._display_name = project_name
		self._description  = description
		self._is_draft     = False
		self._modified     = True
		self.save(c)

		if self._draft_owner.projects_loaded:
			self._draft_owner.get_projects(c).append(self)

	def delete(self, c: Cursor) -> None:
		"""
		Delete this project from the database

		Deletes the project, removing it from the projects list of all owners, and
		deleting all sources.

		WARNING:  This will not update any pre-existing user objects, and so existing
		users will still display the project in their project listings.  Recreate existing
		users to fix this.
		"""
		c.execute('''
			DELETE FROM projects WHERE project_id = %s;
		''', (self._project_id, ))

	@property
	def alphaid(self) -> str:
		"""
		The alphaid of the project

		Every project and user has and ID that is represented to the user as a
		series of one or more letters, from a set of 26 unaccented latin
		characters.  This is calculated from the numeric ID that represents
		each object in the database.

		This method does not validate the the id of the project exists in any
		way other than being a valid number
		"""
		return compute_alphaid(self._project_id)

	@staticmethod
	def from_alphaid(alphaid: str) -> 'Project':
		"""
		Create a project by parsing an alphaid

		Every project and user has and ID that is represented to the user as a
		series of one or more letters, from a set of 26 unaccented latin
		characters.  This is calculated from the numeric ID that represents
		each object in the database.

		This method is a wrapper around the normal project constructor which
		parses an alphaid before creating the project.	Just like the normal
		constructor, this does not validate that the ID exists in the database,
		just that it is properly formed.  The ID will be validated on the next
		database read (NOT write), or if project.load() is called.

		Raises:
			mystic.database.MalformedId: The provided ID was malformed, for
				example because it contained an invalid or capital character,
				or because it was to large.
		"""
		return Project(parse_alphaid(alphaid))

	@staticmethod
	def create_project(
		c: Cursor,
		name: str,
		description: str,
	) -> 'Project':
		"""
		Creates a new project with a given name

		This will automatically create a database entry for the new Project,
		and will return a linked model.  The model will be guaranteed to be
		valid and up to date.  Further database queries will not be performed
		for read-only method calls for this model object.

		Note that the new project will NOT have any owners listed, meaning it
		will become a ghost project unless an owner (presumably the one who
		triggered its creation) is added soon.

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
		"""
		c.execute(
			'INSERT INTO projects(display_name, description) VALUES (%s, %s);',
			(name, description)
		)
		project = Project(c.lastrowid)
		project._display_name = name
		project._description = description
		project._is_draft = False
		project._owners = []
		project._data_sources = list()
		return project

	@staticmethod
	def create_draft(
		c: Cursor,
		draft_owner: 'User',
	) -> 'Project':
		"""
		Creates a new project marked as a draft

		This will automatically create a database entry for the new Project,
		and will return a linked model.  The model will be guaranteed to be
		valid and up to date.  Further database queries will not be performed
		for read-only method calls for this model object.

		The project will have the provided user listed as both the draft owner and the
		single owner of the project.  As this is a draft, no name or description will be
		listed until the draft is finalized.

		For more information about draft projects, see :func:`draft_owner`.

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
			sqlite3.IntegrityError:  The provided user already has a draft, and another
				cannot be created until the first is finalized or deleted.
		"""
		c.execute(
			'INSERT INTO projects(display_name, description, draft_owner) VALUES (%s, %s, %s);',
			('', '', draft_owner.user_id)
		)
		project = Project(c.lastrowid)
		project._display_name = ''
		project._description = ''
		project._is_draft = True
		project._draft_owner = draft_owner
		project._owners = []
		project._data_sources = list()
		project.add_owner(c, draft_owner, False)
		return project

	@staticmethod
	def from_record(
		sql_record: Tuple[int, str, str, Optional[int]]
	) -> 'Project':
		"""
		Constructs a record from an SQL query record

		A properly formatted SQL record returned by a function like `SELECT * FROM
		projects` can be converted into a Project object using this method.  Note that
		while this populates attributes like display name and draft status, it will not
		populate the owners or data sources, which require additional queries.

		No validation whatsoever is performed on the resulting Project

		Raises:
			KeyError:  The provided record did not have the required number of fields
		"""
		project = Project(sql_record[0])
		project._display_name = sql_record[1]
		project._description = sql_record[2]
		project._is_draft = sql_record[3] is not None
		if project._is_draft:
			assert sql_record[3] is not None
			project._draft_owner = User(sql_record[3])
		return project

	@property
	def project_id(self) -> int:
		"""
		Returns the decimal id of the project.

		For the alphaid, which should be what the user sees, use .alphaid
		"""
		return self._project_id

	def get_display_name(self, c: Cursor) -> str:
		"""
		Retrieve the display name of the project

		This MAY trigger a database query if the project name is unknown.

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.NonexistantId:	The project was initialized with an ID
				that was not found
		"""
		if self._display_name is None:
			self.load(c)
		return self._display_name #type: ignore

	def set_display_name(self, name: str) -> None:
		"""
		Change the display name of a project

		This change WILL NOT persist in the database until :func:`save` is called.
		However, future attempts to access the display name *from this instance only* will
		immediately reflect the change.  It is recommended that :func:`save` is called as
		soon after the modification as possible.

		This cannot be used on a draft project, HOWEVER, if this project is not loaded,
		this may fail silently.  If you are not sure if this project is a draft, the only
		way to guarantee success is by calling load() beforehand.

		Raises:
			AttributeError:  This project is a draft, and cannot have its display name set
		"""
		if self._is_draft:
			raise AttributeError
		self._display_name = name
		self._modified = True

	def get_description(self, c: Cursor) -> str:
		"""
		Retrieve a description of the project

		This MAY trigger a database query if the project has not yet been loaded

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.NonexistantId:	The project was initialized with an ID
				that was not found
		"""
		if self._description is None:
			self.load(c)
		return self._description #type: ignore

	def set_description(self, description: str) -> None:
		"""
		Change the description of a project

		This change WILL NOT persist in the database until :func:`save` is called.
		However, future attempts to access the description *from this instance only* will
		immediately reflect the change.  It is recommended that :func:`save` is called as
		soon after the modification as possible.

		This cannot be used on a draft project, HOWEVER, if this project is not loaded,
		this may fail silently.  If you are not sure if this project is a draft, the only
		way to guarantee success is by calling load() beforehand.

		Raises:
			AttributeError:  This project is a draft, and cannot have its description set
		"""
		if self._is_draft:
			raise AttributeError
		self._description = description
		self._modified = True

	def get_draft_owner(self, c: Cursor) -> Optional['User']:
		"""
		If this project is a draft, return the user drafting it

		Draft projects do not have a name or description (attempting to access these
		properties returns an empty string), and only exist in order track the data
		sources and owners of projects the user has not created yet.

		Each user may only have one draft project at a time.  Note that draft projects
		shouldn't be displayed to the user in any way except for automagically
		auto-filling the "New Project" form.

		A project may be created as a draft and then finalized, but a finished project may
		never be converted into a draft, nor can a draft be transfered between users.  The
		"owners" of a draft should be disregarded, and should not be able to see the
		draft.

		Iff this project is not a draft, this method will return None.  Otherwise, it will
		return an unpopulated :class:`User`.

		This MAY trigger a database query if the project has not yet been loaded

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.NonexistantId:	The project was initialized with an ID
				that was not found
		"""
		if self._is_draft is None:
			self.load(c)
			assert self._is_draft is not None
		if self._is_draft:
			assert self._draft_owner is not None
			return self._draft_owner
		else:
			return None

	def is_draft(self, c: Cursor) -> bool:
		"""
		Shorthand for checking if :func:`draft_owner` is not None
		"""
		return self.get_draft_owner(c) is not None

	def get_owners(self, c: Cursor) -> List['User']:
		"""
		Retrieve a list of owners for the project

		This MAY trigger a database query if the owners are unknown.

		This does NOT validate that the project Id corresponds to an actual
		project, and will simply populate with an empty list if that is the
		case.  Note that this is the same behavior as if there are simply no
		owners.

		It's possible that projects without owners may be automatically pruned
		from the database in the future.

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
		"""
		if self._owners is None:
			# TODO we can populate these more
			c.execute('SELECT user_id FROM owners WHERE project_id = %s', (self._project_id,))
			self._owners = [User(uid) for (uid,) in c.fetchall()]
		return self._owners

	def get_data_sources(self, c: Cursor) -> List[Source]:
		"""
		Retrieve the data sources for this project.

		This MAY trigger a database query if the sources are currently unknown.

		This does NOT validate that the project id exists in the database, and
		will simply populate with an empty dict if that is the case.  Note that
		this is the same behavior as if there are simply no sources.

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
		"""
		if self._data_sources is None:
			c.execute('''
				SELECT
					source_id,
					projects.*,
					data_type,
					data_url
				FROM data_sources
				JOIN projects
					ON projects.project_id = data_sources.project_id
				WHERE data_sources.project_id = %s
			''', (self._project_id,))
			records = cast(Tuple[Tuple[int, int, str, str, Optional[int], str, str]], c.fetchall())
			source_tuples = (
				Source(
					record[0], # source_id
					Project.from_record(record[1:-2]),
					record[-2], # data_type
					record[-1], # data_url
				)
				for record in records
			)
			self._data_sources = [
				SOURCE_PROCESSORS[source_type](source_id, project, source_type, url)
				for (source_id, project, source_type, url)
				in source_tuples
			]
		return self._data_sources

	def get_folded_data_sources(self, c: Cursor) -> Dict[Type[Source], List[Source]]:
		def fold_into_dict(d: Dict[Type[Source], List[Source]], s: Source) -> Dict[Type[Source], List[Source]]:
			if type(s) not in d:
				d[type(s)] = [s]
			else:
				d[type(s)].append(s)
			return d

		sources: Dict[Type[Source], List[Source]] = dict()
		return reduce(fold_into_dict, self.get_data_sources(c), sources)

	@property
	def loaded(self) -> bool:
		"""
		Check if basic properties about this Project have been loaded.

		If `True`, then :func:`get_display_name`, :func:`get_description`,
		:func:`get_draft_owner`, and :func:`is_draft` can be called without triggering a
		database query.  This is initially false, but a load can be triggered after an
		attempt is made to access one of these fields, after which they will be
		permanently available.
		"""
		return self._display_name is not None

	@property
	def owners_loaded(self) -> bool:
		"""
		Check if this Project's owners have been loaded.

		If `True`, then :func:`get_owners` can be called without triggering a database
		query.  This is initially `False`, but after the owners have been fetched, they
		are available permanently.
		"""
		return self._owners is not None

	@property
	def data_sources_loaded(self) -> bool:
		"""
		Check if this Project's data sources have been loaded.

		If `True`, then :func:`get_data_sources` can be called without triggering a
		database query.  This is initially `False`, but after the sources have been
		fetched, they are available permanently.
		"""
		return self._data_sources is not None

	def __eq__(self, other: object) -> bool:
		if isinstance(other, Project):
			return self.project_id == other.project_id
		elif isinstance(other, int):
			return self.project_id == other
		elif isinstance(other, str):
			return self.alphaid == other
		else:
			return False

class User:
	def __init__(self, user_id: int) -> None:
		"""
		Initialize a new user model without any of the feilds initialized.
		Relevant fields will be filled lazilly as requested.

		This does not perform any database queries, nor does it validate
		whether the provided id exists in the database.  This can be manually
		checked by calling .load() on the new user.

		Raises:
			mystic.database.MalformedId: The ID of the user is malformed
		"""
		MalformedId.check_malformed(user_id)
		self._user_id:	  int                     = user_id
		self._username:   Optional[str]           = None
		self._first_name: Optional[str]           = None
		self._last_name:  Optional[str]           = None
		self._email:      Optional[str]           = None
		self._projects:   Optional[List[Project]] = None

	def load(self, c: Cursor) -> None:
		"""
		Perform the necessary database query to complete some fields.

		This fills the username field, and in the future, likely some other
		fields that don't required complex database queries.  You will almost
		never have to call this method, as it is automatically called when a
		relevant and unfilled property is accessed.

		However, this method can be used to verify that the user exists in the
		database, as it will return a NonexistantId error in all other
		circumstances.

		Raises:
			sqlite3.OperationalError:  The user was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.NonexistantId:	The user was initialized with an ID
				that was not found
		"""
		c.execute('SELECT * FROM users WHERE user_id = %s;', (self._user_id,))
		record = c.fetchone()
		if record is None:
			raise NonexistantId('user', self._user_id)
		self._username = record[1]
		self._first_name = record[2]
		self._last_name = record[3]
		self._email = record[4]

	def check_owns(self, c: Cursor, project: Project) -> bool:
		"""
		Verify is a given project is owned by the user.

		Returns true if:
			- The user exists
			- The project exists
			- The user is listed as an owner of the project

		Note that if either the user or the project does not exist, this WILL
		NOT produce an error, and will simply return false without being
		detected.

		Raises:
			sqlite3.OperationalError:  The user model was initialized with a
				cursor that points to a database that was not properly set up
		"""
		if self._projects is not None and len(self._projects) == 0:
			# A quick little shortcut that saves us a database query
			return False
		c.execute('''
			SELECT *
			FROM owners
			WHERE user_id = %s
				AND project_id = %s;
		''', (self._user_id, project.project_id))
		return len(c.fetchall()) != 0

	def delete_source_if_owned(self, c: Cursor, src_id: int) -> bool:
		"""
		Deletes a data source after performing permission checks for the given user.

		If a user is listed as an owner to a project, then they are permitted
		to add or remove a source to/from the project.	This method performs
		this check before completing the action.

		If this process was successful, and the user had permission, then
		`true` is returned.

		Raises:
			sqlite3.OperationalError:  The user model was initialized with a
				cursor that points to a database that was not properly set up
		"""
		if self._projects is not None and len(self._projects) == 0:
			# A quick little shortcut that saves us a database query
			return False
		row_count = c.execute('''
			DELETE FROM data_sources
			WHERE source_id = %s
			AND project_id IN (
				SELECT project_id
				FROM owners
				WHERE user_id = %s
			);
		''', (src_id, self._user_id))
		return row_count > 0

	def get_draft_project(self, c: Cursor) -> Project:
		"""
		Get this user's draft project

		Every user has a single draft project, which is not visible in any of the project
		lists.  This project is never shown to any user (until finalized), but is used to
		store things like the data_sources of the project the user has not yet submitted,
		i.e. the one in the "New Project" form.

		This method retrieves this user's draft project from the database, creating it if
		it doesn't exist already.  This method is never cached.

		See :func:`Project.draft_owner` for more information
		"""
		c.execute('''
			SELECT * FROM projects WHERE draft_owner = %s
		''', (self.user_id,))
		maybe_draft = cast(Optional[Tuple[int, str, str, Optional[int]]], c.fetchone())
		if maybe_draft is not None:
			project = Project.from_record(maybe_draft)
			other_self = project.get_draft_owner(c)
			assert other_self is not None
			other_self._projects   = self._projects
			other_self._username   = self._username
			other_self._first_name = self._first_name
			other_self._last_name  = self._last_name
			other_self._email      = self._email
			return project
		else:
			return Project.create_draft(c, self)

	@staticmethod
	def create_user(
		c: Cursor,
		uid: int,
		username: str,
		first_name: str,
		last_name: str,
		email: str,
	) -> 'User':
		"""
		Creates a new user with a given username

		This will automatically register the user in the database, and will
		return a linked model.	The model will be guaranteed to be valid and up
		to date.  Further database queries will not be performed for read-only
		method calls for this model object.

		Note that the new project will NOT have any owners listed, meaning it
		will become a ghost project unless an owner (presumably the one who
		triggered its creation) is added soon.

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.MalformedId: The provided ID was malformed, for
				example because it was negative or because it was to large.
		"""
		c.execute('''
			INSERT INTO users(user_id, username, first_name, last_name, email)
			VALUES (%s, %s, %s, %s, %s);
		''', (uid, username, first_name, last_name, email))
		user = User(c.lastrowid)
		user._projects = []
		user._username = username
		user._first_name = first_name
		user._last_name = last_name
		user._email = email
		return user

	@staticmethod
	def create_or_update(
		c: Cursor,
		uid: int,
		username: str,
		first_name: str,
		last_name: str,
		email: str
	) -> 'User':
		"""
		Update a user's information, creating one if not already in the db

		This is effectively a User.create_user() with no chance of throwing an
		error if the user already exists.  Please read the documentation of
		User.create_user() for more information.

		One key difference between this user and a user created through the
		create_user() method is that this user will not have a pre-populated
		projects attribute, so certain properties will require a database
		connection to access.

		Raises:
			sqlite3.OperationalError:  The project was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.MalformedId: The provided ID was malformed, for
				example because it was negative or because it was to large.
		"""
		c.execute('''
			INSERT INTO users(user_id, username, first_name, last_name, email)
			VALUES (%s, %s, %s, %s, %s)
			ON DUPLICATE KEY UPDATE
				username   = VALUES(username),
				first_name = VALUES(first_name),
				last_name  = VALUES(last_name),
				email      = VALUES(email);
		''', (uid, username, first_name, last_name, email))
		user = User(uid)
		user._username = username
		user._first_name = first_name
		user._last_name = last_name
		user._email = email
		return user

	@staticmethod
	def lookup_user(c: Cursor, username: str) -> Optional['User']:
		"""
		Attempt to fetch a user by their username

		Returns a partially populated user if successful, or none if unsuccessful.

		Raises:
			sqlite3.OperationalError:  The provided cursor points to a database
				that was not properly set up
		"""
		c.execute('SELECT * FROM users WHERE username = %s;', (username,))
		record = c.fetchone()
		if record is None:
			return None
		user = User(record[0])
		user._username = record[1]
		return user

	@property
	def alphaid(self) -> str:
		"""
		The user's alphaid

		Every project and user has and ID that is represented to the user as a
		series of one or more letters, from a set of 26 unaccented latin
		characters.  This is calculated from the numeric ID that represents
		each object in the database.

		This method does not validate the the id of the project exists in any
		way other than being a valid number
		"""
		return compute_alphaid(self._user_id)

	@staticmethod
	def from_alphaid(alphaid: str) -> 'User':
		"""
		Create a user by parsing an alphaid

		Every project and user has and ID that is represented to the user as a
		series of one or more letters, from a set of 26 unaccented latin
		characters.  This is calculated from the numeric ID that represents
		each object in the database.

		This method is a wrapper around the normal user constructor which parses
		an alphaid before initializdng the project.	Just like the normal
		constructor, this does not validate that the ID exists in the database,
		just that it is properly formed.  The ID will be validated on the next
		database read (NOT write), or if user.load() is called.

		Raises:
			mystic.database.MalformedId: The provided ID was malformed, for
				example because it contained an invalid or capital character,
				or because it was to large.
		"""
		return User(parse_alphaid(alphaid))

	@property
	def user_id(self) -> int:
		"""
		Returns the user's numeric id
		"""
		return self._user_id

	def get_username(self, c: Cursor) -> str:
		"""
		Retrieves the user's username

		This MAY require a database query if the field has not yet been fetched

		Raises:
			sqlite3.OperationalError:  The user was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.NonexistantId:	The user was initialized with an ID
				that was not found
		"""
		if self._username is None:
			self.load(c)
		return self._username #type: ignore

	def get_first_name(self, c: Cursor) -> str:
		"""
		Retrieves the user's first name (preferred, not legal)

		This MAY require a database query if the field has not yet been fetched

		Raises:
			sqlite3.OperationalError:  The user was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.NonexistantId:	The user was initialized with an ID
				that was not found
		"""
		if self._first_name is None:
			self.load(c)
			assert self._first_name is not None
		return self._first_name

	def get_last_name(self, c: Cursor) -> str:
		"""
		Retrieves the user's last name

		This MAY require a database query if the field has not yet been fetched

		Raises:
			sqlite3.OperationalError:  The user was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.NonexistantId:	The user was initialized with an ID
				that was not found
		"""
		if self._last_name is None:
			self.load(c)
			assert self._last_name is not None
		return self._last_name

	def get_email(self, c: Cursor) -> str:
		"""
		Retrieves the user's listed email

		This MAY require a database query if the field has not yet been fetched

		Raises:
			sqlite3.OperationalError:  The user was initialized with a
				cursor that points to a database that was not properly set up
			mystic.database.NonexistantId:	The user was initialized with an ID
				that was not found
		"""
		if self._email is None:
			self.load(c)
			assert self._email is not None
		return self._email

	def get_avatar(self, c: Cursor) -> str:
		"""
		Retrieves an avatar link for the user

		This uses the libravatar service
		"""
		h = md5(self.get_email(c).strip().lower().encode('utf-8')).hexdigest()
		return f'http://cdn.libravatar.org/avatar/{h}?d=identicon'

	def get_projects(self, c: Cursor) -> List[Project]:
		"""
		Retrieves a list of projects for which the user is listed as an owner

		This MAY require a database query if the field has not yet been fetched

		This does not verify that the user's id is present in the database, and
		will return an empty list if that is the case.	Note that this is the
		same behavior as if the user simply owned no projects.

		Raises:
			sqlite3.OperationalError:  The user was initialized with a
				cursor that points to a database that was not properly set up
		"""
		if self._projects is None:
			c.execute('''
				SELECT projects.*
				FROM owners
				JOIN projects
					ON owners.project_id = projects.project_id
				WHERE user_id = %s
					AND draft_owner IS NULL
			''', (self._user_id,))
			self._projects = [
				Project.from_record(
					cast(
						Tuple[int, str, str, Optional[int]],
						record
					)
				)
				for record in c.fetchall()
			]
		return self._projects

	@property
	def loaded(self) -> bool:
		"""
		Check if basic properties about this User have been loaded.

		If `True`, then the :func:`get_name` and :func:`get_username` can be called
		without triggering a database query.  This is initially false, but a load can be
		triggered after an attempt is made to access one of these fields, after which they
		will be permanently available.
		"""
		return self._first_name is not None

	@property
	def projects_loaded(self) -> bool:
		"""
		Check if this User's projects have been loaded

		If `True`, then :func:`get_projects` may be called without triggering a database
		query.  This is initially `False`, but once projects are retrieved once, they will
		be permanently available.
		"""
		return self._projects is not None

	def __eq__(self, other: object) -> bool:
		if isinstance(other, User):
			return self.user_id == other.user_id
		elif isinstance(other, int):
			return self.user_id == other
		else:
			return False

class InvalidId(Exception):
	"""An ID was invalid in some way"""
	def __init__(self, item_id: Union[int, str], message: Optional[str] = None) -> None:
		self.item_id = item_id
		self.message = message

	def __str__(self) -> str:
		if self.message is None:
			return f'Invalid ID {self.item_id}'
		else:
			return f'Invalid ID {self.item_id}: {self.message}'

class NonexistantId(InvalidId):
	"""An ID did not point to any relevant object in the database"""
	def __init__(self, item_type: str, item_id: int) -> None:
		super().__init__(item_id)
		self.item_type = item_type

	def __str__(self) -> str:
		return f'No {self.item_type} exists with the ID {self.item_id}'

class MalformedId(InvalidId):
	"""An id, either numeric or an alphaid, was not properly formed"""
	def __init__(self, item_id: Union[str, int], reason: Optional[str] = None) -> None:
		super().__init__(item_id, message = reason)

	@staticmethod
	def check_malformed(some_id: int) -> None:
		"""
		Runs some basic validity checks on an ID.

		Current checks are:
			- No negative IDs
			- No IDs greater than the sqlite3 maximum int size

		If no malformations are detected, the method exits successfully.
		Otherwise, a MalformedId exception is raised.
		"""
		if some_id < 0:
			raise MalformedId(some_id, 'No negative ids are allowed')
		elif some_id > (2 ** 63 - 1):
			raise MalformedId(some_id, 'Id greater than maximum value')
		else:
			return None

	def __str__(self) -> str:
		if self.message is None:
			return f'The ID {self.item_id} is malformed'
		else:
			return f'The ID {self.item_id} is malformed: {self.message}'

def compute_alphaid(val: int) -> str:
	"""
	Convert the given int into base26

	Requires that the input is not negative, and produces output consisting
	exclusively of letters a through z.  This value is used as an alternative
	to a numeric id, especially where it can be seen by the user, and in URLs.

	Raises:
		mystic.database.MalformedId: The ID the project was created with was negative
	"""
	if val < 0:
		raise MalformedId(val, 'Negative IDs cannot be converted into alphaids')
	out: List[str] = []
	while True:
		out.append(_alphabet[val % 26])
		val //= 26
		if val == 0:
			break
	return ''.join(out[::-1])

def parse_alphaid(alphaid: str) -> int:
	"""
	Attempt to convert an alphaid into an integer value

	The input must consist exclusively of lowercase letters a-z, and the output
	will always be a non-negative integer.

	This is effectively just a conversion from base26.

	Raises:
		mystic.database.MalformedId: The ID includes unexpected or capital characters
	"""
	try:
		return sum(
			26 ** place * _alphabet.index(c)
			for (place, c)
			in enumerate(alphaid[::-1])
		)
	except ValueError:
		raise MalformedId(alphaid, "Unexpected character in alphaid")
