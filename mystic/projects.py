from functools import reduce
from mystic.database import Project

from pymysql.cursors import Cursor
from .sources import SOURCE_PROCESSORS, Source

from typing import Dict, List, Optional, Tuple, cast

def produce_project_report(c: Cursor) -> Dict[str, Dict[str, List[str]]]:
	"""
	Produce a `projects.json` file suitable for grimoirelab from the current database

	Most often used with `json.dump` in order to save the report to a file.  Handles
	database connections internally.
	"""
	records = c.execute('''
		SELECT
			data_sources.source_id,
			projects.*,
			data_sources.data_type,
			data_sources.data_url
		FROM data_sources
		JOIN projects
			ON projects.project_id = data_sources.project_id
	''')

	def fold_into_report(
		d: Dict[str, Dict[str, List[str]]],
		t: Source,
	) -> Dict[str, Dict[str, List[str]]]:
		pname = t.project.get_display_name(c)
		if pname not in d:
			d[pname] = {t.source_type: [t.url]}
		else:
			project = d[pname]
			if t.source_type not in project:
				project[t.source_type] = [t.url]
			else:
				project[t.source_type].append(t.url)
		return d

	records = (
		Source(r[0], Project.from_record(cast(Tuple[int, str, str, Optional[int]], r[1:-2])), r[-2], r[-1])
		for r in c.fetchall()
	)

	records = (
		expanded_source_tuple
		for source_tuple
		in records
		for expanded_source_tuple
		in SOURCE_PROCESSORS[source_tuple.source_type](source_tuple.origin_id, source_tuple.project, source_tuple.source_type, source_tuple.url).expand_source()
	)

	report: Dict[str, Dict[str, List[str]]] = {}
	return reduce(fold_into_report, records, report)
