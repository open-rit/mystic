from setuptools import setup

setup(
	name='mystic',
	version='0.1.0',
	packages=['mystic'],
	install_requires=[
		'flask',
		'pymysql',
		'elasticsearch>=6.0.0,<7.0.0',
		'flask-saml2 @ git+https://github.com/Alch-Emi/flask-saml2@timestamps#egg=flask-saml2',
	],
)
